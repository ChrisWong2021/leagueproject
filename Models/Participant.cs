namespace LeagueProject.Models
{
    public class Participant
    {
        public int ParticipantId { get; set; }

        public string ParticipantFirstName  { get; set; }
        public string ParticipantLastName  { get; set; }
    }
}
