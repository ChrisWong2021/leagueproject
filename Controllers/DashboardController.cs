﻿using Microsoft.AspNetCore.Mvc;
using System;

namespace LeagueProject.Controllers
{
    public class DashboardController : Controller
    {

        public IActionResult Dashboard()
        {
            return View();
        }

        [HttpPost]
        public IActionResult PostNewBasketballPlayer2(Models.Participant newParticipant)
        {
            Console.WriteLine(newParticipant);
            return RedirectToAction("Dashboard", "Dashboard");
        }
    }
}
