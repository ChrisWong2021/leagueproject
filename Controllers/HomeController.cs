﻿using System.Collections.Generic;
using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using LeagueProject.Models;

namespace LeagueProject.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            var participants = new List<Participant>()
            {
                new Participant
                {
                    ParticipantId =  1,
                    ParticipantFirstName = "John",
                    ParticipantLastName = "Smith",
                },
                new Participant
                {
                    ParticipantId =  2,
                    ParticipantFirstName = "Foo",
                    ParticipantLastName = "Bar",
                },
                new Participant
                {
                    ParticipantId =  3,
                    ParticipantFirstName = "Hello",
                    ParticipantLastName = "World",
                }
            };
            return View(participants);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
